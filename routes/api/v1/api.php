<?php

use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\Api\V1\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['namespace' => 'Api\V1'], function () {
    Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
        Route::post('register', 'AuthController@register');
        Route::post('login', 'AuthController@login');
    });

    Route::group(['prefix' => 'user', 'namespace' => 'User', 'middleware' => 'auth:sanctum'], function () {
        Route::get('profile', 'UserController@profile');
        Route::get('logout', 'UserController@logout');
    });
});
