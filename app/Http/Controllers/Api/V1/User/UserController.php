<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function profile(Request $request)
    {
        $data = auth()->user();
        return response()->json([
            'status' => true,
            'message' => "Profile data",
            'user' => $data,
        ], 200);
    }
    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete(); //this will work event with this undefine error
        return response()->json([
            'status' => true,
            'message' => "User logged out successfully",
        ], 200);
    }
}
