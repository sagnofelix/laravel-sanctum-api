<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Helpers\Helpers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => ['required', Password::min(6)],
        ], [
            'name.required' => 'The first name field is required.',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        $user = new User;

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);

        $user->save();

        return response()->json([
            'status' => true,
            'message' => "User created successfully",
        ], 200);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => ['required', Password::min(6)],
        ], [
            'name.required' => 'The first name field is required.',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        $user = User::where([
            ['email', $request->email],
        ])->first();

        if (!empty($user)) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken("UserAuthToken")->plainTextToken;
                return response()->json([
                    'status' => true,
                    'message' => "Login successfully",
                    'token' => $token,
                ], 200);
            }

            return response()->json([
                'status' => false,
                'message' => "Incorrect password",
            ], 401);
        }

        return response()->json([
            'status' => false,
            'message' => "Invalid credentials",
        ], 404);
    }
}
